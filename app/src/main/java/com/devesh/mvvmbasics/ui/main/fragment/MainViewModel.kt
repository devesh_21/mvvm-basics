package com.devesh.mvvmbasics.ui.main.fragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.devesh.mvvmbasics.data.repository.MainRepository
import com.devesh.mvvmbasics.data.room.entity.ResultsItem
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class MainViewModel @Inject constructor (private val repository: MainRepository) : ViewModel() {

    fun getMovieReviews() = liveData(Dispatchers.IO) {
        emit(UiState.Loading)
        try {
            emit(UiState.Success(data = repository.getMovieReviews()))
        } catch (e: Exception) {
            emit(UiState.Error)
        }
    }

    sealed class UiState {
        object Loading : UiState()
        class Success(val data: LiveData<List<ResultsItem>>): UiState()
        object Error: UiState()
    }
}